import pandas as pd
import os

file_path = os.getcwd()+'/ml_model_training/production_data/'


class LoadFiles:

    xgb_clf_model_path = file_path+'xgb_clf_1.4.model'
    y_test_pred = pd.read_pickle(
        file_path+'y_test_pred.pkl')
    y_train_pred = pd.read_pickle(
        file_path+'y_train_pred.pkl')
    y_test = pd.read_pickle(
        file_path+'y_test.pkl')
    y_train = pd.read_pickle(
        file_path+'y_train.pkl')

    country_to_encoded_value_map = pd.read_pickle(
        file_path+'country_to_encoded_value_map.pkl')
    customer_language_to_encode_value_map = pd.read_pickle(
        file_path+'customer_language_to_encode_value_map.pkl')
    eoncde_value_to_customer_language_map = {
        encode_value: cus_lang for cus_lang, encode_value in customer_language_to_encode_value_map.items()}
    ver_lang_to_encode_map = pd.read_pickle(
        file_path+'ver_lang_to_encode_map.pkl')
    eoncde_value_to_ver_lang_map = {
        encode_value: ver_lang for ver_lang, encode_value in ver_lang_to_encode_map.items()}
    genre_to_encoded_value_map = pd.read_pickle(
        file_path+'genre_to_encoded_value_map.pkl')
    genre_to_encoded_value_map.pop('Select')

    top_items_per_user_country_genre_category = pd.read_pickle(
        file_path+'top_items_per_user_country_genre_category.pkl')
    top_simillar_items_per_user_country_genre_category = pd.read_pickle(
        file_path+'top_simillar_items_per_user_country_genre_category.pkl')

    episodeVersion_to_title_map = pd.read_pickle(
        file_path+'episodeVersion_to_title_map.pkl')
    seriesCollection_to_title_map = pd.read_pickle(
        file_path+'seriesCollection_to_title_map.pkl')

    item_type_to_encoded_value_map = pd.read_pickle(
        file_path+'item_type_to_encoded_value_map.pkl')
    production_columns = pd.read_pickle(file_path+'production_columns.pkl')
    features = pd.read_pickle(file_path+'features.pkl')

    contrib_index_map = pd.read_pickle(
        file_path+'contrib_index_map.pkl')
    top_simillar_users_per_genre_and_contrib = pd.read_pickle(
        file_path+'top_simillar_users_per_genre_and_contrib.pkl')
    top_users_per_genre_and_contrib = pd.read_pickle(
        file_path+'top_users_per_genre_and_contrib.pkl')
    customer_to_name_map = pd.read_pickle(file_path+'customer_to_name_map.pkl')
    # reversing the customer_to_name_map
    name_to_customer_map = {user_name: user_id for user_id,
                            user_name in customer_to_name_map.items()}

    user_lookup_data_dtypes = pd.read_pickle(
        file_path+'user_lookup_data_dtypes.pkl')
    item_lookup_data_dtypes = pd.read_pickle(
        file_path+'item_lookup_data_dtypes.pkl')

    # data files
    pipeline_data = pd.read_excel(file_path+'Pipeline-17052021.xlsx', usecols=[
        'Catalogue ID', 'Programme Type'])
    user_lookup_data = pd.read_csv(
        file_path+'user_lookup_data.csv', dtype=user_lookup_data_dtypes)
    all_seriesCollections_with_series_data = pd.read_csv(
        file_path+'all_seriesCollections_with_series_data.csv', dtype=item_lookup_data_dtypes)
    all_episodeVersions_with_series_data = pd.read_csv(
        file_path+'all_episodeVersions_with_series_data.csv', dtype=item_lookup_data_dtypes)
    user_item_class_label_data = pd.read_csv(
        file_path+'user_item_class_label_data.csv')
    all_contents_mapped_to_series_data = pd.read_csv(
         file_path+'all_contents_mapped_to_series_data.csv')

    avails_api_language = {"English": 29, "Abkhazian": 150, "Afrikaans": 2, "Albanian": 3, "Amharic": 118, "Arabic": 5, "Armenian": 134, "Azerbaijani": 135, "Basque": 151, "Belarusian": 136, "Bengali": 8, "Bosnian": 10, "Bulgarian": 11, "Burmese": 12, "Cantonese": 13, "Catalan": 15, "Chamorro": 152, "Croatian": 24, "Czech": 25, "Danish": 26, "Dari": 153, "Dutch": 27, "Dzongkha": 142, "Estonian": 34, "Faroese": 148, "Farsi": 35, "Filipino": 154, "Finnish": 36, "Flemish": 37, "French": 38, "Gaelic": 114, "Georgian": 43, "German": 44, "Gilbertese": 155, "Greek": 48, "Greenlandic": 156, "Hakka": 157, "Hebrew": 50, "Hindi": 51, "Hungarian": 52, "Icelandic": 53, "Indonesian": 54, "Italian": 56, "Japanese": 57, "Kazakh": 128, "Khmer": 59, "Kirundi": 158, "Korean": 60, "Kurdish": 61, "Lao": 62, "Latvian": 64, "Lithuanian": 65, "Macedonian": 66, "Malay": 67, "Malayalam": 68, "Malaysian": 159, "Maldivian": 145, "Maltese": 147, "Mandarin": 69, "Mongolian": 119, "Montenegrin": 197, "Nauruan": 160, "Nepali": 198, "Norwegian": 74, "Pashto": 140, "Persian": 146, "Pitkern": 163, "Polish": 78, "Portuguese": 79, "Punjabi": 82, "Pushto": 139, "Romanian": 83, "Russian": 84, "Samoan": 122, "Sango": 165, "Serbian": 85, "Sesotho": 166, "Sinhalese": 149, "Slovak": 89, "Slovenian": 90, "Somali": 121, "Spanish": 91, "Swahili": 97, "Swazi": 168, "Swedish": 98, "Taiwanese Hokkien": 199, "Tamil": 100, "Thai": 102, "Tokelauan": 125, "Tongan": 169, "Tsonga": 170, "Tswana": 171, "Turkish": 104, "Turkmen": 132, "Tuvaluan": 127, "Ukrainian": 105, "Urdu": 108, "Uzbek": 133, "Venda": 172, "Vietnamese": 109, "Welsh": 110, "Xhosa": 173, "Yiddish": 111, "Zulu": 113}

    avails_api_country = {"Algeria, People's Democratic Republic of": 31, "Egypt, Arab Republic of": 32, "Libya, State of": 33, "Morocco, Kingdom of": 34, "Sudan, Republic of (North Sudan)": 35, "Tunisia, Republic of": 36, "Western Sahara": 37, "Eastern Africa": 24, "Central Africa": 25, "Southern Africa": 26, "Western Africa": 27, "Anguilla": 91, "Antigua and Barbuda": 92, "Aruba": 93, "Bahamas, Commonwealth of the": 94, "Barbados": 95, "Bonaire, Sint Eustatius and Saba": 96, "British Virgin Islands": 97, "Cayman Islands": 98, "Cuba, Republic of": 99, "Curaçao": 100, "Dominica, Commonwealth of": 101, "Dominican Republic": 102, "Grenada": 103, "Guadeloupe": 104, "Haiti, Republic of": 105, "Jamaica": 106, "Martinique": 107, "Montserrat": 108, "Puerto Rico, Commonwealth of": 109, "Saint Barthelemy, Territorial Collectivity of": 110, "Saint Kitts and Nevis (Federation of Saint Christopher and Nevis)": 111, "Saint Lucia": 112, "Saint Martin (French Part)": 113, "Saint Vincent and the Grenadines": 114, "Sint Maarten (Dutch Part)": 115, "Trinidad and Tobago, Republic of": 116, "Turks and Caicos Islands (TCI)": 117, "United States Virgin Islands (USVI)": 118, "Central America": 28, "South America": 29, "Bermuda": 145, "Greenland": 358, "Canada": 143, "United States of America (USA)": 144, "Saint Pierre and Miquelon, Overseas Collectivity of": 146, "Kazakhstan, Republic of": 147, "Kyrgyzstan (Kyrgyz Republic)": 148, "Tajikistan, Republic of": 149, "Turkmenistan": 150, "Uzbekistan, Republic of": 151, "China, People's Republic of (PRC)": 152, "Hong Kong, SAR China": 153, "Japan": 157, "Korea, Democratic People's Rep. of (North Korea, DPRK)": 156, "Korea, Republic of (South Korea)": 159, "Macao, SAR China": 154, "Mongolia": 158, "Taiwan (Republic of China, ROC)": 155, "Brunei Darussalam (Nation of Brunei, the Abode of Peace)": 160, "Cambodia, Kingdom of": 161, "Indonesia, Republic of": 162, "Laos (Lao People's Democratic Republic)": 163, "Malaysia": 164, "Myanmar, Republic of the Union of (Burma)": 165, "Philippines, Republic of the": 166, "Singapore, Republic of": 167, "Thailand, Kingdom of": 168, "Timor-Leste, Democratic Republic of (East Timor)": 169, "Vietnam, Socialist Republic of": 170, "Afghanistan, Islamic Republic of": 171, "Bangladesh, People's Republic of": 172, "Bhutan, Kingdom of": 173, "India, Republic of": 174, "Iran, Islamic Republic of (Persia)": 175, "Maldives, Republic of": 176, "Nepal, Federal Democratic Republic of": 177, "Pakistan, Islamic Republic of": 178, "Sri Lanka, Democratic Socialist Republic of": 179, "Armenia, Republic of": 180, "Azerbaijan, Republic of": 181, "Bahrain, Kingdom of": 182, "Cyprus, Republic of": 183, "Georgia": 184, "Iraq, Republic of": 185, "Israel, State of": 186, "Jordan, Hashemite Kingdom of": 187, "Kuwait, State of": 188, "Lebanon (Lebanese Republic)": 189, "Oman, Sultanate of": 190, "Palestinian territories, Occupied (OPT)": 193, "Qatar, State of": 191, "Saudi Arabia, Kindgom of (KSA)": 192, "Syrian Arab Republic (Syria)": 194, "Turkey, Republic of": 195, "United Arab Emirates (UAE)": 196, "Yemen, Republic of": 197, "Belarus, Republic of": 198, "Bulgaria, Republic of": 199, "Czech Republic (Czechia)": 200, "Hungary": 201, "Poland, Republic of": 202, "Moldova, Republic Of": 203, "Romania": 204, "Russian Federation (Russia)": 205, "Slovak Republic (Slovakia)": 206, "Ukraine": 207, "Aland Islands": 208, "Denmark, Kingdom of": 210, "Estonia, Republic of": 211, "Faroe Islands": 212, "Finland, Republic of": 213, "Iceland": 214, "Latvia, Republic of": 217, "Lithuania, Republic of": 218, "Norway, Kingdom of": 219, "Svalbard and Jan Mayen": 220, "Sweden, Kingdom of": 221, "United Kingdom of Great Britain and Northern Ireland (UK)": 222, "Guernsey": 224, "Ireland (Eire)": 215, "Isle of Man": 216, "Jersey, Bailiwick of": 225, "Albania, Republic of": 227, "Andorra, Principality of": 228, "Bosnia and Herzegovina": 229, "Croatia, Republic of": 230, "Gibraltar": 231, "Greece (Hellenic Republic)": 232, "Holy See (Vatican City State)": 235, "Italy (Italian Republic)": 234, "Malta, Republic of": 236, "Montenegro": 237, "Portugal (Portuguese Republic)": 238, "San Marino, Republic of": 239, "Serbia, Republic of": 240, "Slovenia, Republic of": 241, "Spain, Kingdom of": 242, "Macedonia, Republic of": 243, "Kosovo, Republic of": 244, "Austria, Republic of": 246, "Belgium, Kingdom of": 247, "France (French Republic)": 248, "Germany, Federal Republic of": 249, "Liechtenstein, Principality of": 250, "Luxembourg, Grand Duchy of": 251, "Monaco, Principality of": 252, "Netherlands (Holland)": 253, "Switzerland (Swiss Federation)": 254, "Australia, Commonwealth of": 255, "Christmas Island, Territory of": 256, "Cocos (Keeling) Islands, Territory of": 257, "Heard Island and McDonald Islands, Territory of (HIMI)": 258, "New Zealand": 259, "Norfolk Island": 260, "Fiji, Republic of": 261, "New Caledonia": 262, "Papua New Guinea, Independent State of (PNG)": 263, "Solomon Islands": 264, "Vanuatu, Republic of": 265, "Guam": 266, "Kiribati, Republic of": 267, "Marshall Islands, Republic of the": 268, "Micronesia, Federated States of (FSM)": 269, "Nauru, Republic of": 270, "Northern Mariana Islands, Commonwealth of the (CNMI)": 271, "Palau, Republic of": 272, "United States Minor Outlying Islands": 273, "American Samoa": 274, "Cook Islands": 275, "French Polynesia": 276, "Niue": 277, "Pitcairn Islands": 278, "Samoa, Independent State of": 279, "Tokelau": 280, "Tonga, Kingdom of": 281, "Tuvalu": 282, "Wallis and Futuna Islands, Territory of": 283}

    rl_country_list = ['China', 'Belgium', 'Canada', 'Netherlands', 'United Kingdom', 'Iceland', 'USA', 'Australia', 'Germany', 'Italy', 'France', 'New Zealand', 'Philippines', 'United Arab Emirates', 'Sweden', 'Ireland', 'Luxembourg', 'South Korea', 'Turkey', 'Uruguay', 'Czech Republic', 'Singapore', 'India', 'Japan', 'Spain', 'Poland', 'Malaysia', 'Qatar', 'Romania', 'Hong Kong', 'Russian Federation', 'Greece', 'British Virgin Islands', 'Norway', 'Thailand', 'Argentina', 'Hungary', 'Panama', 'Mexico', 'Israel', 'Nepal', 'Slovenia', 'South Africa', 'Brazil', 'Estonia', 'Mauritius', 'Taiwan', 'Switzerland', 'Azerbaijan', 'Bosnia and Herzegovina', 'Slovakia', 'Finland', 'Bangladesh', 'Egypt', 'Kuwait', 'Cyprus', 'Barbados', 'Serbia', 'Denmark', 'Bulgaria', 'Latvia', 'Cambodia', 'Chile', 'Colombia', 'Trinidad and Tobago', 'Lebanon', 'Portugal', 'Costa Rica', 'Ecuador', 'Peru', 'Guatemala', 'Honduras', 'Nicaragua', 'Vietnam', 'Nigeria', 'Georgia', 'Ukraine', 'Burma', 'Moldova', 'Kazakhstan', 'Malta', 'Croatia', 'Mongolia', 'Pakistan', 'Austria', 'Lithuania', 'Macedonia', 'Belize', 'Albania', 'Kenya', 'Kyrgyzstan', 'Indonesia', 'Morocco', 'Armenia', 'Brunei Darussalam']

    new_country_codes = {}
    for c in rl_country_list:
        keys = []
        for key, val in avails_api_country.items():
            if c in key:
                keys.append(val)
        if len(keys)>0:
            new_country_codes[c] = keys  
