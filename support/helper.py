import pandas as pd
import numpy as np
from xgboost.sklearn import XGBClassifier
import streamlit as st
from support.load import LoadFiles as lf
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns
import base64
from time import gmtime, strftime
from support.api_requests import API_Request as api
import os


class SupportMethods:

    xgb_clf_model_path = lf.xgb_clf_model_path
    features = lf.features
    y_train_pred = lf.y_train_pred
    y_test_pred = lf.y_test_pred
    y_train = lf.y_train
    y_test = lf.y_test
    contrib_index_map = lf.contrib_index_map
    ver_lang_to_encode_map = lf.ver_lang_to_encode_map
    encode_value_to_ver_language_map = {
        encode: language for language, encode in ver_lang_to_encode_map.items()}
    eoncde_value_to_customer_language_map = lf.eoncde_value_to_customer_language_map
    eoncde_value_to_ver_lang_map = lf.eoncde_value_to_ver_lang_map
    country_to_encoded_value_map = lf.country_to_encoded_value_map
    encode_value_to_country_map = {
        encode: country for country, encode in country_to_encoded_value_map.items()}
    customer_language_to_encode_value_map = lf.customer_language_to_encode_value_map
    genre_to_encoded_value_map = lf.genre_to_encoded_value_map
    encoded_value_to_genre_map = {
        encode: genre for genre, encode in genre_to_encoded_value_map.items() if genre != 'Select'}
    top_items_per_user_country_genre_category = lf.top_items_per_user_country_genre_category
    top_simillar_items_per_user_country_genre_category = lf.top_simillar_items_per_user_country_genre_category
    top_simillar_users_per_genre_and_contrib = lf.top_simillar_users_per_genre_and_contrib
    top_users_per_genre_and_contrib = lf.top_users_per_genre_and_contrib
    episodeVersion_to_title_map = lf.episodeVersion_to_title_map
    seriesCollection_to_title_map = lf.seriesCollection_to_title_map
    item_type_to_encoded_value_map = lf.item_type_to_encoded_value_map
    production_columns = lf.production_columns
    name_to_customer_map = lf.name_to_customer_map
    customer_to_name_map = lf.customer_to_name_map
    user_lookup_data = lf.user_lookup_data
    all_seriesCollections_with_series_data = lf.all_seriesCollections_with_series_data
    all_episodeVersions_with_series_data = lf.all_episodeVersions_with_series_data
    user_item_class_label_data = lf.user_item_class_label_data
    all_contents_mapped_to_series_data = lf.all_contents_mapped_to_series_data
    pipeline_data = lf.pipeline_data
    seriesCollection_and_episodeVersion_to_title_map = dict(list(lf.episodeVersion_to_title_map.items())+list(
        lf.seriesCollection_to_title_map.items()))

    extract_country_codes = lf.new_country_codes
    avails_api_language = lf.avails_api_language

    all_titles_map = {**lf.seriesCollection_to_title_map,
                      **lf.episodeVersion_to_title_map}

    # train_df.total_stering_amount_sum.mean()
    total_stering_amount_sum = 0.08816955983638763

    # load saved model
    xgb_clf = XGBClassifier()
    xgb_clf.load_model(xgb_clf_model_path)

    def company_dropbox():
        company_values = [SupportMethods.customer_to_name_map[user]
                          for user in SupportMethods.user_item_class_label_data.user_id.value_counts().index]
        company_values.insert(0, '<select company>')
        company = st.sidebar.selectbox(
            'Select a Company',
            company_values,
            index=company_values.index('<select company>'))
        return company

    def selected_genre_dropbox(genre_values):
        genre_values.insert(0, '<select genre>')
        if 'Select' in genre_values:
            genre_values.remove('Select')
        genre = st.sidebar.selectbox(
            'Select a Genre',
            genre_values,
            index=genre_values.index('<select genre>'))
        return genre

    def genre_dropbox():
        indexes = SupportMethods.user_item_class_label_data.Genres_0_Name.value_counts().index
        genre_values = [SupportMethods.encoded_value_to_genre_map[idx]
                        for idx in indexes if idx in SupportMethods.encoded_value_to_genre_map.keys()]
        genre_values.insert(0, '<select genre>')
        if 'Select' in genre_values:
            genre_values.remove('Select')
        genre = st.sidebar.selectbox(
            'Select a Genre',
            genre_values,
            index=genre_values.index('<select genre>'))
        return genre

    def country_dropbox():
        country_values = [SupportMethods.encode_value_to_country_map[u]
                          for u in SupportMethods.user_item_class_label_data.customer_country.value_counts().index]
        if 'nan' in country_values:
            country_values.remove('nan')
        country_values.insert(0, '<select country>')
        country = st.sidebar.selectbox(
            'Select a Country',
            country_values,
            index=country_values.index('<select country>'))
        return country

    def content_dropbox():
        content_values = ['Series Collection', 'Episode Version']
        content_values.insert(0, '<select content>')
        content = st.sidebar.selectbox(
            'Select a Content',
            content_values,
            index=content_values.index('<select content>'))
        return content

    def recommend_items_for(sample_user_id, recommend_for='Existing Company', seriesCollection_ids=[], episodeVersion_ids=[], top_items=10):

        lang_codes = {'EN': 'English', 'FR': 'French', 'DE': 'German', 'ES': 'Spanish',
                      'PL': 'Polish', 'SL': 'Slovenian', 'PT': 'Portuguese', 'NaN': 'NaN'}

        item_purchase_count = dict(
            SupportMethods.user_item_class_label_data.item_id.value_counts())

        recomended_items = {}

        customer_language = SupportMethods.user_item_class_label_data[SupportMethods.user_item_class_label_data.user_id == sample_user_id].customer_language.unique()[
            0]

        customer_country = SupportMethods.user_item_class_label_data[SupportMethods.user_item_class_label_data.user_id == sample_user_id].customer_country.unique()[
            0]

        dim2_country = SupportMethods.extract_country_codes[
            SupportMethods.encode_value_to_country_map[customer_country]]

        dim3_language = SupportMethods.avails_api_language[lang_codes[
            SupportMethods.eoncde_value_to_customer_language_map[customer_language]]]

        if lang_codes[
                SupportMethods.eoncde_value_to_customer_language_map[customer_language]] == 'English':
            st.warning('Avails - Availabiltiy Status based on "{0}" Country and for "English" Language'.format(
                SupportMethods.encode_value_to_country_map[customer_country]))
        else:
            st.warning('Avails - Availabiltiy Status based on "{0}" Country and for "English" Language & "{1}" Customer Language'.format(SupportMethods.encode_value_to_country_map[customer_country], lang_codes[
                SupportMethods.eoncde_value_to_customer_language_map[customer_language]]))

        for content, data in {'Series Collection': SupportMethods.all_seriesCollections_with_series_data, 'Episode Version': SupportMethods.all_episodeVersions_with_series_data}.items():

            users, items, = [], []
            filtered_ids = []

            if (recommend_for == 'Pipeline') & (content == 'Series Collection'):
                filtered_ids = set(data[data.item_id.isin(
                    seriesCollection_ids)]['item_id'].values)
            elif (recommend_for == 'Pipeline') & (content == 'Episode Version'):
                filtered_ids = set(data[data.item_id.isin(
                    episodeVersion_ids)]['item_id'].values)
            elif recommend_for == 'Existing Company':
                filtered_ids = set(data.item_id.values)

            sample_user_prchased_items = SupportMethods.user_item_class_label_data[(
                SupportMethods.user_item_class_label_data.user_id == sample_user_id)]['item_id'].values
            sample_user_not_purchased_items = list(
                filtered_ids - set(sample_user_prchased_items))

            users.extend(np.zeros(len(sample_user_not_purchased_items),
                                  dtype=np.uint32)+sample_user_id)
            items.extend(sample_user_not_purchased_items)

            items_not_purchased_by_user_data = pd.DataFrame({'user_id': users,
                                                            'item_id': items,
                                                             'total_stering_amount_sum': SupportMethods.total_stering_amount_sum,
                                                             })

            items_not_purchased_by_user_data = items_not_purchased_by_user_data.merge(
                SupportMethods.user_lookup_data, on='user_id').merge(data, on='item_id')[SupportMethods.production_columns]

            items_not_purchased_by_user_data['y_pred'] = SupportMethods.xgb_clf.predict_proba(
                items_not_purchased_by_user_data.drop(['user_id', 'item_id', ], axis=1))[:, 1]

            top_recommended_items = items_not_purchased_by_user_data.sort_values(
                by='y_pred', ascending=False)[['item_id', 'Genres_0_Name', 'version_language', 'y_pred']]

            if content == 'Series Collection':
                recomended_items[content] = {}
                for genre in SupportMethods.genre_to_encoded_value_map.keys():
                    recomended_items[content][genre] = (
                        np.array([(top_recommended_items['y_pred'].iloc[idx] * item_purchase_count.get(item, 1),  "{0} ({1})".format(SupportMethods.seriesCollection_to_title_map[item], SupportMethods.eoncde_value_to_ver_lang_map[top_recommended_items[top_recommended_items.item_id == item]['version_language'].unique()[
                                 0]]), item, dim2_country) for idx, item in enumerate(top_recommended_items[top_recommended_items['Genres_0_Name'] == SupportMethods.genre_to_encoded_value_map[genre]]['item_id'].values[:top_items])]),

                        np.array([(top_recommended_items['y_pred'].iloc[idx] * item_purchase_count.get(item, 1), SupportMethods.seriesCollection_to_title_map[item], item, dim2_country, dim3_language) for idx, item in enumerate(top_recommended_items[(top_recommended_items.version_language == customer_language) & (top_recommended_items['Genres_0_Name'] == SupportMethods.genre_to_encoded_value_map[genre])]['item_id'].values[:top_items])]))
            elif content == 'Episode Version':
                recomended_items[content] = {}
                for genre in SupportMethods.genre_to_encoded_value_map.keys():
                    recomended_items[content][genre] = (
                        np.array([(top_recommended_items['y_pred'].iloc[idx] * item_purchase_count.get(item, 1), "{0} ({1})".format(SupportMethods.episodeVersion_to_title_map[item], SupportMethods.eoncde_value_to_ver_lang_map[top_recommended_items[top_recommended_items.item_id == item]['version_language'].unique()[
                                 0]]), item, dim2_country) for idx, item in enumerate(top_recommended_items[top_recommended_items['Genres_0_Name'] == SupportMethods.genre_to_encoded_value_map[genre]]['item_id'].values[:top_items])]),

                        np.array([(top_recommended_items['y_pred'].iloc[idx] * item_purchase_count.get(item, 1), SupportMethods.episodeVersion_to_title_map[item], item, dim2_country, dim3_language) for idx, item in enumerate(top_recommended_items[(top_recommended_items.version_language == customer_language) & (top_recommended_items['Genres_0_Name'] == SupportMethods.genre_to_encoded_value_map[genre])]['item_id'].values[:top_items])]))

        return recomended_items

    def sales_by_type(data, map_data, top=15, fig_size=(8, 5)):
        name = [map_data[u] for u in data.value_counts()[:top].index]
        counts = data.value_counts()[:top].values

        # Figure Size
        fig, ax = plt.subplots(figsize=fig_size)

        # Horizontal Bar Plot
        ax.barh(name, counts)

        # Remove axes splines
        for s in ['top', 'bottom', 'left', 'right']:
            ax.spines[s].set_visible(False)

        # Remove x, y Ticks
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')

        # Add padding between axes and labels
        ax.xaxis.set_tick_params(pad=5)
        ax.yaxis.set_tick_params(pad=10)

        # Show top values
        ax.invert_yaxis()

        # Add annotation to bars
        for i in ax.patches:
            plt.text(i.get_width()+0.2, i.get_y()+0.5,
                     str(round((i.get_width()), 2)),
                     fontsize=10, fontweight='bold',
                     color='grey')

        # Show Plot
        st.pyplot(fig)

    def quantiles_with_values(data, fig_size=(6, 3)):
        no_of_items_purchased_per_user_test = data.groupby(
            by='user_id')['class_label'].count().sort_values(ascending=False)
        quantiles = no_of_items_purchased_per_user_test.quantile(
            np.arange(0, 1.01, 0.01), interpolation='higher')

        # Figure Size
        fig, ax = plt.subplots(figsize=fig_size)

        plt.title("Quantiles and their Values")
        quantiles.plot()
        # quantiles with 0.05 difference
        ax.scatter(x=quantiles.index[::5], y=quantiles.values[::5],
                   c='orange', label="quantiles with 0.05 intervals")
        # quantiles with 0.25 difference
        ax.scatter(x=quantiles.index[::15], y=quantiles.values[::15],
                   c='m', label="quantiles with 0.10 intervals")
        plt.ylabel('No of items purchased by user')
        plt.xlabel('Value at the quantile')
        plt.legend(loc='best')

        # Add x, y gridlines
        ax.grid(b=True, color='grey',
                linestyle='-.', linewidth=0.5,
                alpha=0.2)

        # annotate the 25th, 50th, 75th and 100th percentile values....
        for x, y in zip(quantiles.index[::15], quantiles[::15]):
            ax.annotate(s="({} , {})".format(x, y), xy=(x, y),
                        xytext=(x-0.05, y+100))

        st.pyplot(fig)

    def plot_confusion_matrix(test_y, predict_y, fig_size=(20, 5)):
        C = confusion_matrix(test_y, predict_y)

        A = (((C.T)/(C.sum(axis=1))).T)*100

        B = (C/C.sum(axis=0))*100

        # Figure Size
        fig, ax = plt.subplots(1, 3, figsize=fig_size)

        labels = [0, 1]
        # representing A in heatmap format
        cmap = sns.light_palette("blue")
        plt.subplot(1, 3, 1)
        sns.heatmap(C, annot=True, cmap=cmap, fmt=".2f",
                    xticklabels=labels, yticklabels=labels, annot_kws={'size': 16})
        plt.xlabel('Predicted Class', fontsize=18)
        plt.ylabel('Original Class', fontsize=18)
        plt.title("Confusion matrix", fontsize=18)

        plt.subplot(1, 3, 2)
        sns.heatmap(B, annot=True, cmap=cmap, fmt=".2f",
                    xticklabels=labels, yticklabels=labels, annot_kws={'size': 16})
        plt.xlabel('Predicted Class', fontsize=18)
        plt.ylabel('Original Class', fontsize=18)
        plt.title("Precision matrix", fontsize=18)

        plt.subplot(1, 3, 3)
        # representing B in heatmap format
        sns.heatmap(A, annot=True, cmap=cmap, fmt=".2f",
                    xticklabels=labels, yticklabels=labels, annot_kws={'size': 16})
        plt.xlabel('Predicted Class', fontsize=18)
        plt.ylabel('Original Class', fontsize=18)
        plt.title("Recall matrix", fontsize=18)

        st.pyplot(fig)

    def set_page_title(title):
        st.sidebar.markdown(unsafe_allow_html=True, body=f"""
            <iframe height=0 srcdoc="<script>
                const title = window.parent.document.querySelector('title') \

                const oldObserver = window.parent.titleObserver
                if (oldObserver) {{
                    oldObserver.disconnect()
                }} \

                const newObserver = new MutationObserver(function(mutations) {{
                    const target = mutations[0].target
                    if (target.text !== '{title}') {{
                        target.text = '{title}'
                    }}
                }}) \

                newObserver.observe(title, {{ childList: true }})
                window.parent.titleObserver = newObserver \

                title.text = '{title}'
            </script>" />
        """)

    @st.cache(allow_output_mutation=True)
    def get_base64_of_bin_file(bin_file):
        with open(bin_file, 'rb') as f:
            data = f.read()
        return base64.b64encode(data).decode()

    @st.cache(allow_output_mutation=True)
    def get_img_with_href(object_to_download, download_filename, title):
        if isinstance(object_to_download, pd.DataFrame):
            object_to_download = object_to_download.to_csv(index=False)

            # some strings <-> bytes conversions necessary here
            b64 = base64.b64encode(object_to_download.encode()).decode()

        html_code = f'''
            <div>
            <a href="data:file/txt;base64,{b64}" download="{download_filename}">{title}</a>
            </div>'''
        return html_code

    def make_recommendataion(prediction_for='Existing Company', seriesCollection_ids=[], episodeVersion_ids=[]):
        company = SupportMethods.company_dropbox()
        if company != '<select company>':
            if prediction_for == 'Pipeline':
                recomended_items = SupportMethods.recommend_items_for(
                    SupportMethods.name_to_customer_map[company], prediction_for, seriesCollection_ids, episodeVersion_ids)
            elif prediction_for == 'Existing Company':
                recomended_items = SupportMethods.recommend_items_for(
                    SupportMethods.name_to_customer_map[company], prediction_for)

            customer_language = SupportMethods.eoncde_value_to_customer_language_map[SupportMethods.user_item_class_label_data[SupportMethods.user_item_class_label_data.user_id == SupportMethods.name_to_customer_map[company]].customer_language.unique()[
                0]]

            y_pred, content_type_array, genre_type_array, item_data_array, availability_status, content_ids, dim2, dim3 = [
            ], [], [], [], [], [], [], set()

            for content_type, genre_data in recomended_items.items():
                for genre_type, items_data in genre_data.items():
                    if len(items_data[0]) > 0:
                        for pred, item_title, item, dim2_country in items_data[0]:
                            content_type_array.append(
                                '{0}'.format(content_type))
                            genre_type_array.append(genre_type)
                            item_data_array.append(item_title)
                            y_pred.append(float(pred))
                            content_ids.append(item)
                            dim2.extend(dim2_country)
                            dim3.add(29)

                        if (customer_language != 'EN') and (len(items_data[1]) > 0):
                            for pred, item_title, item, dim2_country, dim3_language in items_data[1]:
                                content_type_array.append(
                                    '{0}'.format(content_type))
                                genre_type_array.append(genre_type)
                                item_data_array.append('{0} ({1})'.format(
                                    item_title, customer_language))
                                y_pred.append(float(pred))
                                content_ids.append(item)
                                dim2.extend(dim2_country)
                                dim3.add(dim3_language)

            avails_df = SupportMethods.get_content_availibiliy(
                list(set(content_ids)), list(set(dim2)), list(dim3))

            availability_status = ['Available' if i in avails_df.catalogueID.unique(
            ) else 'Unavailable' for i in content_ids if list(content_ids)]

            recomender_df = pd.DataFrame({'Rank': np.around(y_pred), 'Content Type': content_type_array,
                                          'Genre': genre_type_array, 'Catalogue ID': content_ids, 'Title': item_data_array, 'Availability Status': availability_status})

            sorted_recomender_df = recomender_df.copy().sort_values('Rank', ascending=False)[
                ['Content Type', 'Genre', 'Catalogue ID', 'Title', 'Availability Status']]

            sorted_recomender_df.reset_index(drop=True, inplace=True)

            # saving the files for next time recommendation we need to remove these content_ids
            if prediction_for == 'Pipeline':
                sorted_recomender_df.to_csv(os.getcwd()+'/processed_files/Pipleline_Content/{0}_recommendataions_{1}.csv'.format(
                    company, strftime("%d_%m_%Y_%H%M%S", gmtime())))
            elif prediction_for == 'Existing Company':
                sorted_recomender_df.to_csv(os.getcwd()+'/processed_files/Existing_Company/{0}_recommendataions_{1}.csv'.format(
                    company, strftime("%d_%m_%Y_%H%M%S", gmtime())))

            recomender_df_download_link = SupportMethods.get_img_with_href(
                sorted_recomender_df, '{0}_recommendataions({1})({2}).csv'.format(company, prediction_for, strftime("%d-%m-%Y %H:%M:%S", gmtime())), 'Download Recommendataions Report')
            st.markdown(recomender_df_download_link, unsafe_allow_html=True)

            avails_df_download_link = SupportMethods.get_img_with_href(
                avails_df, '{0}_avails_availability({1})({2}).csv'.format(company, prediction_for, strftime("%d-%m-%Y %H:%M:%S", gmtime())), 'Download Avails Report')
            st.markdown(avails_df_download_link, unsafe_allow_html=True)

            placeholder = st.empty()

            # if st.button('ShowAll'):
            #      placeholder.table(sorted_recomender_df)

            placeholder.table(sorted_recomender_df[(
                sorted_recomender_df['Availability Status'] == 'Available')])

            content = SupportMethods.content_dropbox()
            genre = SupportMethods.genre_dropbox()
            if (content != '<select content>') & (genre != '<select genre>'):
                placeholder.table(sorted_recomender_df[(sorted_recomender_df['Availability Status'] == 'Available') & (sorted_recomender_df['Content Type'] == content) & (
                    sorted_recomender_df['Genre'] == genre)])
            elif (content != '<select content>'):
                placeholder.table(
                    sorted_recomender_df[(sorted_recomender_df['Availability Status'] == 'Available') & (sorted_recomender_df['Content Type'] == content)])
            elif (genre != '<select genre>'):
                placeholder.table(
                    sorted_recomender_df[(sorted_recomender_df['Availability Status'] == 'Available') & (sorted_recomender_df['Genre'] == genre)])

    def get_content_availibiliy(content_ids, dim2, dim3):

        access_key, secret_key, securityToken, expiration = api.temporary_credentials()
        data = api.get_item_availabiliy(
            access_key, secret_key, securityToken, content_ids, [16, 4], dim2, dim3).json()
        avails_report = {'catalogueID': [], 'content_status': [], 'start_date': [
        ], 'end_date': [], 'media': [], 'territory': [], 'language': [], 'exclusivity': []}
        for row in data['rows']:
            avails_report['catalogueID'].append(row['id'])
            avails_report['content_status'].append(row['status']['statusName'])
            avails_report['start_date'].append(row['windowStart'])
            avails_report['end_date'].append(row['windowEnd'])
            avails_report['media'].append(
                '; '.join([i['value'] for i in row['dim1']]))
            avails_report['territory'].append(
                '; '.join([i['value'] for i in row['dim2']]))
            avails_report['language'].append(
                '; '.join([i['value'] for i in row['dim3']]))
            avails_report['exclusivity'].append(
                'Exclusive' if row['isExclusive'] else 'Non-Exclusive')
        return pd.DataFrame(avails_report)
