import base64
import requests
import sys
import datetime
import streamlit as st
from requests_auth_aws_sigv4 import AWSSigV4


class API_Request:

    env = 'stage'
    baseAddress = 'https://api.{0}.bbcstudios.com/content/v1'.format(env)
    consumer_secret = 'Au0uIZ8hrgwMPiy5kVmRMEDR+q23S1ggZ4dJpDUBM'
    consumer_key = '501f1702-9cc2-44d9-b2fb-cfd424e3a4ce'
    ocpApimSubscriptionKey = 'bfaa91adc8804f5e922293ae6ed8440a'
    response_status_code = 0

    def get_access_token():
        consumer_key_secret = API_Request.consumer_key+":"+API_Request.consumer_secret
        consumer_key_secret_enc = base64.b64encode(
            consumer_key_secret.encode()).decode()

        headersAuth = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + str(consumer_key_secret_enc),
        }

        data = {
            'Grant_Type': 'client_credentials'
        }

        # Authentication request

        response = requests.post(
            'https://api.{0}.bbcstudios.com/login/v1/oauth2/token'.format(API_Request.env), headers=headersAuth, data=data, verify=True)

        return response.json()['access_token']

    def get_genre_and_contributors_api_data(content_id):
        access_token = 'Bearer {0}'.format(API_Request.get_access_token())
        my_headers = {'Authorization': access_token,
                      'Ocp-Apim-Subscription-Key': API_Request.ocpApimSubscriptionKey}
        epv_contrib_response = requests.get(
            '{0}/episodeVersions/{1}'.format(API_Request.baseAddress, content_id), headers=my_headers)

        sc_contrib_response = requests.get(
            '{0}/seriesCollections/{1}'.format(API_Request.baseAddress, content_id), headers=my_headers)

        if epv_contrib_response.status_code == 200:
            API_Request.response_status_code = 200
            epv_response_json = epv_contrib_response.json()
            epv_contrib_list = ["{0} {1}".format(contrib['attributes']['firstName'], contrib['attributes']['lastName'])
                                for contrib in epv_response_json['included'] if contrib['type'] == 'contributor']

            episode_id = epv_response_json['data']['relationships']['episode']['data']['id']
            episode_res = requests.get(
                '{0}/episodes/{1}'.format(API_Request.baseAddress, episode_id), headers=my_headers).json()
            configuration_id = episode_res['data']['relationships']['configuration']['data']['id']
            configuration_res = requests.get(
                '{0}/configurations/{1}'.format(API_Request.baseAddress, configuration_id), headers=my_headers).json()
            series_id = configuration_res['data']['relationships']['series']['data']['id']
            series_res = requests.get(
                '{0}/series/{1}'.format(API_Request.baseAddress, series_id), headers=my_headers).json()
            production_genre = series_res['data']['attributes']['genres'][0]['name']
            return ['{0} :: {1}'.format(production_genre, epv) for epv in epv_contrib_list]

        elif sc_contrib_response.status_code == 200:
            API_Request.response_status_code = 200
            sc_response_json = sc_contrib_response.json()
            sc_contrib_list = ["{0} {1}".format(contrib['attributes']['firstName'], contrib['attributes']['lastName'])
                               for contrib in sc_response_json['included'] if contrib['type'] == 'contributor']

            configuration_id = sc_response_json['data']['relationships']['configuration']['data']['id']
            configuration_res = requests.get(
                '{0}/configurations/{1}'.format(API_Request.baseAddress, configuration_id), headers=my_headers).json()
            series_id = configuration_res['data']['relationships']['series']['data']['id']
            series_res = requests.get(
                '{0}/series/{1}'.format(API_Request.baseAddress, series_id), headers=my_headers).json()
            production_genre = series_res['data']['attributes']['genres'][0]['name']
            return ['{0} :: {1}'.format(production_genre, sc) for sc in sc_contrib_list]

    def get_contributors_list(content_id):
        response = API_Request.get_content_api_data(content_id)
        response_json = response.json()
        API_Request.response_status_code = response.status_code
        if response.status_code == 200:
            contrib_list = ["{0} {1}".format(contrib['attributes']['firstName'], contrib['attributes']['lastName'])
                            for contrib in response_json['included'] if contrib['type'] == 'contributor']
        return contrib_list


#########
#RL AWS Request
#########


    def temporary_credentials():
        # This portion of the code sends a request to the Rightsline API for temporary AWS credentials. These are single use credentials.
        url = "https://api-staging.rightsline.com/v3/auth/temporary-credentials"
        json_to_send = "{\n\t\"accessKey\":\"%s\",\n\t\"secretKey\":\"%s\"\n}" % (
            '1ugJkLtaCtJ+8trKWmmTNAqHPu2cLPRT9tg5IruI9mQ=', 'zqrYVt0WjsN6yLPRCa2NnwU67SpHnleOypRG8KwXBtw=')

        # These are the headers that are sent with the initial AWS temporary credentials
        initial_headers = {
            'content-type': "application/json",
            'x-api-key': '9gwx4HqaVZ9AZHNBy9vJj1TTb9yTkvfd2XFcwUI8'
        }

        response = requests.post(
            url, headers=initial_headers, data=json_to_send, verify=True).json()
        return response["accessKey"], response["secretKey"], response["sessionToken"], response["expiration"]

    access_key, secret_key, securityToken, expiration = temporary_credentials()

    def get_item_availabiliy(access_key, secret_key, securityToken, content_id, dim1_media, dim2_country, dim3_language):

        service = 'execute-api'
        region = "us-east-1"
        endpoint = 'https://api-staging.rightsline.com/v3/avails/availability'
        x_api_key = '9gwx4HqaVZ9AZHNBy9vJj1TTb9yTkvfd2XFcwUI8'

        t = datetime.datetime.utcnow()
        amzdate = t.strftime('%Y%m%dT%H%M%SZ')

        if access_key is None or secret_key is None:
            print('No access key is available.')
            sys.exit()

        # Request parameters for Avails -> "is-title-available".
        request_parameters = '{'
        request_parameters += '"recordId": {0},'.format(content_id)
        request_parameters += '"dim1": {0},'.format(dim1_media)
        request_parameters += '"dim2": {0},'.format(dim2_country)
        request_parameters += '"dim3": {0},'.format(dim3_language)
        request_parameters += '"windowStart": "{0}",'.format(
            t.strftime('%Y-%m-%d'))
        request_parameters += '"windowEnd": "{0}",'.format('2021-12-31')
        request_parameters += '"matchType": "{0}",'.format('OverlapPart')
        request_parameters += '"start": {0},'.format(0)
        request_parameters += '"rows": {0}'.format(100)
        request_parameters += '}'

        aws_auth = AWSSigV4(service=service, region=region, amzdate=amzdate,
                            x_api_key=x_api_key,
                            aws_access_key_id=access_key,
                            aws_secret_access_key=secret_key,
                            aws_session_token=securityToken
                            )
        headers = {"x-amz-date": amzdate, "x-api-key": x_api_key,
                   "content-type": "application/json"}

        res = requests.post(endpoint, data=request_parameters,
                            headers=headers, auth=aws_auth)

        return res
