import streamlit as st
from support.helper import SupportMethods as sm
from multiapp import MultiApp
# import your app modules here
from apps import machine_learning_process_flow, for_pipeline_series_content, for_existing_company, for_a_new_company, for_a_new_content
from PIL import Image

favicon = Image.open('favicon.jpg')
st.set_page_config(
    page_title="ContentMatchRS",
    page_icon=favicon,
    layout="wide",
)

app = MultiApp()

hide_streamlit_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_streamlit_style, unsafe_allow_html=True)

sm.set_page_title('ContentMatchRS')
st.sidebar.title("ContentMatchRS")
st.sidebar.markdown("*Version 1.4*")

# Add all your application here
app.add_app("Machine Learning - Process Flow",
            machine_learning_process_flow.app)
app.add_app("For Pipeline Series Content", for_pipeline_series_content.app)
app.add_app("For Existing Company", for_existing_company.app)
app.add_app("For a New Company", for_a_new_company.app)
app.add_app("For a New Content", for_a_new_content.app)


# The main app
app.run()
