from support.helper import SupportMethods as sm
import streamlit as st
import pandas as pd
from time import gmtime, strftime
import os

def app():
    st.title('Recommend Content to a New Company')
    st.markdown(unsafe_allow_html=True, body="""<pre>Choose the Country: 
    It will list the top purchased contents. 
    Also, will predict the top purchased similar contents.</pre>""")

    country = sm.country_dropbox()
    #content = sm.content_dropbox()
    if (country != '<select country>'):
   
        based_on, content_type_array, genre_type_array, content_ids = [], [], [], []
        for key, content_data in {'Top Items':sm.top_items_per_user_country_genre_category[country], 'Top Simillar Items':sm.top_simillar_items_per_user_country_genre_category[country]}.items():
            for content_type, genre_data in content_data.items():
                for genre_type, items_data in genre_data.items():
                    if len(items_data)>0:
                        for item in items_data:
                            based_on.append(key)
                            content_type_array.append(content_type)
                            genre_type_array.append(genre_type)
                            content_ids.append(item)

        items_title_array = [sm.all_titles_map[item] for item in content_ids]
        dim2_country = sm.extract_country_codes[country]

        dim3_language = 4 #all languages

        st.warning('Avails - Availabiltiy Status based on "{0}" Country and for "All Languages"'.format(country))

        avails_df = sm.get_content_availibiliy(list(set(content_ids)), dim2_country, dim3_language)
        
        availability_status = ['Available' if i in avails_df.catalogueID.unique() else 'Unavailable' for i in content_ids if list(content_ids)]

        recomender_df = pd.DataFrame({'Content Type': content_type_array,
                                      'Genre': genre_type_array, 'Catalogue ID': content_ids , 'Title': items_title_array, 'Availability Status': availability_status})

        # saving the files for next time recommendation we need to remove these content_ids
        recomender_df.to_csv(os.getcwd()+'/processed_files/New_Company/{0}_recommendataions_{1}.csv'.format(
            country, strftime("%d_%m_%Y_%H%M%S", gmtime())))

        tmp_download_link = sm.get_img_with_href(
                recomender_df, '{0}_recommendataions_for_new_company({1}).csv'.format(country, strftime("%d-%m-%Y %H:%M:%S", gmtime())), 'Download Recommendataions Report')
        st.markdown(tmp_download_link, unsafe_allow_html=True)

        avails_df_download_link = sm.get_img_with_href(
                avails_df, '{0}_avails_availability({1}).csv'.format(country, strftime("%d-%m-%Y %H:%M:%S", gmtime())), 'Download Avails Report')
        st.markdown(avails_df_download_link, unsafe_allow_html=True)

        placeholder = st.empty()
            
        # if st.button('ShowAll'):
        #      placeholder.table(sorted_recomender_df)
            
        placeholder.table(recomender_df[(recomender_df['Availability Status'] == 'Available')])
    

        if len(set(content_type_array)) == 1:
            content = list(set(content_type_array))[0]
        else:
            content = sm.content_dropbox()
        selected_genres = list(set(genre_type_array))
        genre = sm.selected_genre_dropbox(selected_genres)
        if (content != '<select content>') & (genre != '<select genre>'):
                placeholder.table(recomender_df[(recomender_df['Content Type'] == content) & (
                        recomender_df['Genre'] == genre)])
        elif (content != '<select content>'):
                placeholder.table(
                    recomender_df[(recomender_df['Content Type'] == content)])
        elif (genre != '<select genre>'):
                placeholder.table(recomender_df[(recomender_df['Genre'] == genre)])
