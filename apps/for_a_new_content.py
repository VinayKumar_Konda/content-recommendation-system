from support.helper import SupportMethods as sm
import streamlit as st
from support.api_requests import API_Request as api
from collections import OrderedDict
from collections import Counter
import pandas as pd
from time import gmtime, strftime

def app():
    st.title('Recommend New Content to a Company')
    st.markdown(unsafe_allow_html=True, body="""<pre>Enter the New catalog id: 
    It will list the top purchased companies based on Contributors. 
    Also, will predict the top purchased similar companies based on Contributors""")
    
    #content = sm.content_dropbox()
    catalog_id = st.sidebar.text_input(
        'Enter Catalog Id: ', help='Enter SeriesCollection or EpisodeVersion Catalog Id')

    if (len(catalog_id)>0):
        genre_contrib_array, user_array = [], []
        production_genre_contrib_list = api.get_genre_and_contributors_api_data(catalog_id)
        
        if api.response_status_code == 200:
            for data in [sm.top_users_per_genre_and_contrib, sm.top_simillar_users_per_genre_and_contrib]:
                for genre_contrib in production_genre_contrib_list:
                    genre_contrib_array.extend(data.get(genre_contrib, ''))
                contrib_sorted_dict = OrderedDict(sorted(Counter(genre_contrib_array).items(),
                        key=lambda x: x[1], reverse=True))
                user_array.extend([uid for uid in list(contrib_sorted_dict.keys())])
        
            unique_ids = list(OrderedDict(sorted(Counter(user_array).items(),
                            key=lambda x: x[1], reverse=True)).keys())
            user_names = [sm.customer_to_name_map[uid] for uid in unique_ids]
            recomender_df = pd.DataFrame({'Company Id': unique_ids, 'Company Name': user_names}) 
                    
            tmp_download_link = sm.get_img_with_href(recomender_df, '({0})_recommendataions_for_new_content({1}).csv'.format(
                catalog_id, strftime("%d-%m-%Y %H:%M:%S", gmtime())), 'Download Recommendataions Report')
            st.markdown(tmp_download_link, unsafe_allow_html=True)
                
            placeholder = st.empty()
            placeholder.table(recomender_df)
            st.write(sm.user_item_class_label_data[(sm.user_item_class_label_data['user_id'].isin(unique_ids)) & (sm.user_item_class_label_data['item_id'] == int(
                catalog_id))]['user_id'].nunique())

        else:
            st.error("""
            No record found on Content-API for neither **Episode Version** nor **Series Collection** with:

            Catalog Id : **{0}**
            
            """.format(catalog_id))
        