from support.helper import SupportMethods as sm
import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import xgboost as xgb
from sklearn.metrics import f1_score

def app():
    st.title('Machine Learning - Process Flow')
    st.header('1. Problem Statement')
    st.markdown("""
    To help customers to find top similar **EpisodeVersion** & **SeriesCollection** contents, **BBCS** developed content recommender system: **ContentMatchRS**. Its job is to predict the probability of a content based on respecitve customer sales history and can use those predictions to make personal content recommendations. 
    """)

    st.header('2. Mapping the problem to a Machine Learning Problem')
    st.subheader('2.1 Type of Machine Learning Problem')
    st.markdown("""
    For a given **Customer** and **Content** we need to predict the probablity that customers might like the content. The given problem is a Recommendation problem and it can also seen as a Classification problem.
    """)
    st.subheader('2.2 Performance Metric')
    st.markdown("""
    1. Both precision and recall is important so F1 score is good choice: https://en.wikipedia.org/wiki/F-score
    2. Confusion Matrix: https://en.wikipedia.org/wiki/Confusion_matrix
    """)
    st.subheader('2.3 Machine Learning Objective and Constraints')
    st.markdown("""
    1. **Maximize** the F1-score.
    2. Try to provide some interpretability.
    """)

    st.header('3. Data Aquistion')
    st.markdown(
        "1. Get the 'Approved' deals data of 'Active' customers from Rightsline")
    st.image('./images/legacy_deals.jpg')
    st.markdown(
        "2. Get the 'Catalog_LINK_Catalog' data from RDW_DIV38_STG and create content mappings of all levels")
    st.image('./images/content_mapping.jpg')
    st.markdown("3. Get the high-level data of Series from Content-API db")
    st.image('./images/series_data.jpg')
    st.markdown("4. Get the 'Version Creation Type' & 'Version Language' data of SeriesCollection and EpisodeVersion from Content-API db")
    st.markdown("5. Get the 'contact_name', 'sap_cstomer_number', 'customer_group', 'customer_city', 'customer_country', 'customer_active_salesorg', 'proforma_customer', 'customer_language', 'customer_risk_class' data of Customer from Rightsline & Salesforce")
    st.image('./images/customer_data.jpg')

    st.header('4. Data Preparation')
    st.markdown("""
    1. Map the "total_ster_amount_dict" to "legacy_deals" data
    2. Merge both Legacy and New rightsline deals
    >> total # of records: **803557** 
    3. Group the data by 'customer_id' & 'item_id'
    >> total # of records after grouping: **630102** 
    4. Label the purchased data with '1'
    5. Create dummy data which matches the records count of purchased data
    6. Concatenate label "1"(purchased data) and "0"(dummy data) records  
    >> total # of records with dummy data: **1260201** 
    7. Map the "concatenated_data" data with series_id
    8. Map the "all_deals_with_series_id" data with "version_creation_type" & "version_language"
    9. Map high level series data with "all_deals_with_series_id"
    10. Map the customer data with "all_deals_with_series_data"
    11. Filter the SeriesCollections from contents_mapping_data and map high level series,  "version_creation_type" & "version_language" data
    12. Filter the EpisodeVersions from contents_mapping_data and map high level series, "version_creation_type" & "version_language" data
    """)

    st.header('5. Data Pre-processing')
    st.markdown("""
    1. Basic Statistics (#Labels, #Customers, and #Items)
    
    > **Total data** 
    > - Total no of Labels : 1228069
    > - Total No of Customers : 1681
    > - Total No of Items : 66745
    
    2. Spliting the data into Train and Test (75:25) ratio
    
    > **Training data** 
    > - Total no of labels : 921051
    > - Total No of Users   : 1681
    > - Total No of items  : 66740

    > **Test data** 
    > - Total no of labels : 307018
    > - Total No of Users   : 1680
    > - Total No of Items  : 64032
    """)
    st.markdown("**Distribution of Class Labels '1' and '0'**")
    st.image('./images/distribution_of_class_label.jpg')

    st.markdown(
        "3. Create vectorization of Categorical Features using LabelEncoder")
    st.code("""
    Exmaple Code

    >>> le = preprocessing.LabelEncoder()
    >>> le.fit(["paris", "paris", "tokyo", "amsterdam"])
    >>> list(le.classes_)
    ['amsterdam', 'paris', 'tokyo']
    >>> le.transform(["tokyo", "tokyo", "paris"])
    array([2, 2, 1]...)
    >>> list(le.inverse_transform([2, 2, 1]))
    ['tokyo', 'tokyo', 'paris']
    """)

    st.markdown(
        "4. Create vectorization of Numerical Features by Imputing missing values and performing Standardization")
    st.code("""
    Exmaple Code
    
    >>> from sklearn.preprocessing import StandardScaler
    >>> data = [[0, 0], [0, 0], [1, 1], [1, 1]]
    >>> scaler = StandardScaler()
    >>> scaler.fit(data)
    >>> scaler.transform(data)
    [[-1. -1.], [-1. -1.], [ 1.  1.], [ 1.  1.]]
    """)

    st.header("6. Data Analysis")
    st.markdown("**Sales by Content Type**")
    st.image('./images/sales_by_content_types.jpg')
    st.markdown("**Sales by Companies - Top 15**")
    sm.sales_by_type(sm.user_item_class_label_data.user_id,
                     sm.customer_to_name_map)
    sm.quantiles_with_values(sm.user_item_class_label_data, fig_size=(10, 6))
    st.markdown("**Sales by Countries - Top 15**")
    sm.sales_by_type(sm.user_item_class_label_data.customer_country,
                     sm.encode_value_to_country_map)
    st.markdown("**Sales by Genre**")
    sm.sales_by_type(sm.user_item_class_label_data.Genres_0_Name,
                     sm.encoded_value_to_genre_map, top=10, fig_size=(6, 4))
    st.markdown("**Sales by Content Version Language**")
    sm.sales_by_type(sm.user_item_class_label_data.version_language,
                     sm.encode_value_to_ver_language_map, fig_size=(6, 4))

    st.header('7. Model Training')
    st.markdown("Got the best parameters to train the model, after hyperparameter tuning")
    st.markdown(sm.xgb_clf.get_params())
    st.subheader('7.1 F1-Score')
    st.write('Train f1 score',round(f1_score(sm.y_train, sm.y_train_pred)*100,4))
    st.write('Test f1 score',round(f1_score(sm.y_test, sm.y_test_pred)*100,4))

    st.subheader('7.2 Plot Important Features')
    fig, ax = plt.subplots(figsize=(10, 8))
    sm.xgb_clf.get_booster().feature_names = sm.features
    xgb.plot_importance(sm.xgb_clf, ax=ax, max_num_features=15, grid=False, title='Top 15 Features')
    st.pyplot(fig)

    st.subheader('7.3 Confusion Matrix')
    st.markdown('Train confusion_matrix')
    sm.plot_confusion_matrix(sm.y_train, sm.y_train_pred)
    st.markdown('Test confusion_matrix')
    sm.plot_confusion_matrix(sm.y_test, sm.y_test_pred)
