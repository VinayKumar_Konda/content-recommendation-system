from support.helper import SupportMethods as sm
import streamlit as st
import pandas as pd


def app():
    st.title('Recommend Content Based on Pipeline Series Data')
    st.markdown(unsafe_allow_html=True, body="""<pre>Upload the Pipeline sheet and choose the Company: 
    It will predict the top content preferences along with versrion language. 
    Also, will predict the top content preferences based on Compnay language, 
        if the Company language is non-English.</pre>""")


    uploaded_file = st.sidebar.file_uploader("Choose a file")

    if uploaded_file is None:
        pipeline_df = sm.pipeline_data
    else:
        pipeline_df = pd.read_excel(uploaded_file, usecols=[
                                    'Catalogue ID', 'Programme Type'])
    
    series_ids = pipeline_df[pipeline_df['Programme Type']
                                 == 'Series']['Catalogue ID'].unique()

    need_to_update_data = list(set(series_ids) - set(sm.all_contents_mapped_to_series_data.series_id.unique()))

    if len(need_to_update_data) > 0:
        #st.markdown(need_to_update_data)
        pd.to_pickle(need_to_update_data,
                    '../need_to_update_data.pkl')

    data0 = sm.all_contents_mapped_to_series_data[sm.all_contents_mapped_to_series_data.series_id.isin(
            series_ids)]

    seriesCollection_ids = data0[data0.content_type ==
                                     'Series Collection']['content_id'].unique()
    episodeVersion_ids = data0[data0.content_type ==
                                   'Episode Version']['content_id'].unique()

    sm.make_recommendataion('Pipeline', seriesCollection_ids, episodeVersion_ids)